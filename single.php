
<?php get_header();?>
        <div class="content">
            <div class="container">
			<?php if (have_posts()):while (have_posts()): the_post();?>
			<!-- START: PAGE CONTENT -->			
			<div class="row animate-up">
				<div class="col-sm-8">
					<main class="post-single">
						<article class="post-content section-box">							
							<div class="post-inner">
								<header class="post-header">
									<div class="post-data">
										<div class="post-tag">
											<a href="single.html">#Photo</a>
											<a href="single.html">#Architect</a>
										</div>

										<div class="post-title-wrap">
									
											<h1 class="post-title"><?php the_title() ?></h1>
											<time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
												<span class="day"><?php echo get_the_date('d') ?> </span>
												<span class="month"><?php echo get_the_date('M') ?> </span>
											</time>
										</div>

										<div class="post-info">
											<a href="single.html"><i class="rsicon rsicon-user"></i>ADMIN</a>
											<a href="single.html"><i class="rsicon rsicon-comments"></i>56</a>
										</div>
									</div>
								</header>
								<div class="post-editor clearfix">
						<?php the_content()  ?>
								</div>

								<footer class="post-footer">
									<div class="post-share">
										<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
										<div class="addthis_sharing_toolbox"></div>
									</div>
								</footer>
							</div><!-- .post-inner -->
						</article><!-- .post-content -->

						<nav class="post-pagination section-box">
							<div class="post-next">
								<div class="post-tag">Previous Article <a href="single.html">#Tagapple</a></div>
								<h3 class="post-title"><a href="single.html">How ipsum project web dolor sit amet</a></h3>

								<div class="post-info">
									<a href="single.html"><i class="rsicon rsicon-user"></i>by admin</a>
									<a href="single.html"><i class="rsicon rsicon-comments"></i>56</a>
								</div>
							</div>
							<div class="post-prev">
								<div class="post-tag">Next Article <a href="single.html">#Photography</a></div>
								<h3 class="post-title"><a href="single.html">project web dolor sit amet</a></h3>

								<div class="post-info">
									<a href="single.html"><i class="rsicon rsicon-user"></i>by admin</a>
									<a href="single.html"><i class="rsicon rsicon-comments"></i>56</a>
								</div>
							</div>
						</nav><!-- .post-pagination -->

						<div class="post-comments">
							<h2 class="section-title">Comments (59)</h2>
							
							<div class="section-box">
								<ol class="comment-list">
									<li class="comment">
										<article class="comment-body">
											<div class="comment-avatar">
										
											</div>
											<div class="comment-content">
												<div class="comment-meta">
													<span class="name">J<?php the_author() ?></span>
													<time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
													<a class="reply-link" href="single.html#comment-reply">Reply</a>
												</div>
												<div class="comment-message">
													<p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
														assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
												</div>
											</div>
										</article>

										<ol>
											<li class="comment">
												<article class="comment-body">
													<img class="comment-avatar" src="img/rs-avatar-64x64.jpg" alt="avatar"/>
													<div class="comment-content">
														<div class="comment-meta">
															<span class="name"><?php the_author() ?></span>
															<time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
															<a class="reply-link" href="single.html#comment-reply">Reply</a>
														</div>
														<div class="comment-message">
															<p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
																assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
														</div>
													</div>
												</article>

												<ol>
													<li class="comment">
														<article class="comment-body">
															<img class="comment-avatar" src="img/rs-avatar-64x64.jpg" alt="avatar"/>
															<div class="comment-content">
																<div class="comment-meta">
																	<span class="name"><?php the_author() ?></span>
																	<time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
																	<a class="reply-link" href="single.html#comment-reply">Reply</a>
																</div>
																<div class="comment-message">
																	<p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
																		assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
																</div>
															</div>
														</article>
													</li><!-- .comment (level 3) -->
												</ol><!-- .comment-list (level 3) -->
											</li><!-- .comment (level 2) -->
										</ol><!-- .comment-list (level 2) -->
									</li><!-- .comment -->

									<li class="comment">
										<article class="comment-body">
											<img class="comment-avatar" src="img/rs-avatar-64x64.jpg" alt="avatar"/>
											<div class="comment-content">
												<div class="comment-meta">
													<span class="name">Jane Doe</span>
													<time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
													<a class="reply-link" href="single.html#comment-reply">Reply</a>
												</div>
												<div class="comment-message">
													<p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
														assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
												</div>
											</div>
										</article>
									</li><!-- .comment -->
								</ol><!-- .comment-list -->

								<div id="comment-reply" class="comment-reply">
									<form>
										<div class="input-field">
											<input type="text" name="rs-comment-name"/>
											<span class="line"></span>
											<label>Name *</label>
										</div>

										<div class="input-field">
											<input type="email" name="rs-comment-email"/>
											<span class="line"></span>
											<label>Email *</label>
										</div>

										<div class="input-field">
											<input type="text" name="rs-comment-website"/>
											<span class="line"></span>
											<label>Website</label>
										</div>

										<div class="input-field">
											<textarea rows="4" name="rs-comment-message"></textarea>
											<span class="line"></span>
											<label>Type Comment Here *</label>
										</div>

										<div class="text-right">
											<span class="btn-outer btn-primary-outer ripple">
												<input class="btn btn-lg btn-primary" type="button" value="Leave Comment">
											</span>
										</div>
									</form>
								</div><!-- .comment-reply -->
							</div><!-- .section-box -->
						</div><!-- .post-comments -->
					</main>
					<!-- .post-single -->
				</div>
	<?php endwhile; endif; ?>
				<?php get_sidebar();?>
			
		<?php get_footer();?>


